require "uri"
require "file_utils"
require "http"
require "xml"
require "json"
require "option_parser"

require "./misc.cr"
require "./service.cr"
require "./services/*"

urls = [] of String
safe = debug = false
path = Dir.current
formatter = ""

if ARGV.empty?
  arguments = ["-h"]
else
  arguments = ARGV
end

OptionParser.parse(arguments) do |p|
  p.banner = "dig up all the media."

  p.on("-h", "--help", "show this help"
  ) {
    puts p
    exit 0
  }
  p.on("-d", "--debug", "print src and dst, then exit") { debug = true }
  p.on("--unsafe", "set stricter formatting") { safe = false }
  p.on("-u <url>",
    "--url <url>",
    "specify media url"
  ) { |url| urls << url }

  p.on("-p <path>",
    "--path <path>",
    "specify a path to download to"
  ) { |p| path = p }

  p.on("-f <formatter>",
    "--format <formatter>",
    "specify filename formatter"
  ) { |f| formatter = f }
end

urls.each do |url|
  serv = Service.services.find(&.valid_target? url)
  serv.try &.instance.download url do |url, info|
    info.safe = safe
    info.formatter = formatter unless formatter.empty?
    info.formatter = File.join({path, info.formatter})

    if debug
      puts url + "\n" + info.format
      next
    end

    download url, info.format
  end
end
