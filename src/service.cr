module Service
  class_property services = [] of Service::Base.class

  abstract class Base
    macro inherited
      Service.services << self

      def self.instance
        @@instance ||= new
      end
    end

    # abstract def download(url, &block)
  end

  struct Info
    property formatter, info, safe

    def initialize(@info : Hash(String, String),
                   @formatter : String,
                   @safe = true)
    end

    def format(formatter = @formatter)
      info = @info

      if (safe = @safe)
        illegal = /[^A-Za-z0-9-_.]/
        formatter.gsub(" ", "_")
      else
        illegal = /[\/]/
      end

      formatter.scan(/%\[(\w+)\]/).map(&.[1])
        .each do |match|
          if match
            unless info[match]?
              puts "no such key as: #{match}"
              puts
              puts "available formatting keys:"
              puts "available formatting keys:"
              return ""
            end

            formatter = formatter
              .gsub("%[#{match}]", info[match].to_s.gsub(illegal, "_"))
          else
            break
          end
        end

      return formatter
    end
  end
end
