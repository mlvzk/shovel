class ArtStation < Service::Base
  HOST = "https://www.artstation.com"

  def self.valid_target?(url)
    url.includes? "artstation.com"
  end

  def get_user_projects(username : String)
    (uri = URI.parse HOST).path = "/users/#{username}/projects.json"


    i = 0
    loop do
      uri.query = HTTP::Params.encode({"page" => (i += 1).to_s})
      projects = JSON.parse( HTTP::Client.get(uri).body )

      break if projects["data"].as_a.empty?

      projects["data"].as_a.each do |project|
        get_assets project["hash_id"].to_s do |url, info|
          yield url, info
        end
      end
    end
  end

  def get_assets(hash_id)
    (uri = URI.parse HOST).path = "/projects/#{hash_id}.json"
    project = JSON.parse(retry { HTTP::Client.get(uri).body })

    project["assets"].as_a.each_with_index do |asset, i|
      puts project["user"]["full_name"].to_s + " - " + project["title"].to_s

      if project["assets"].as_a.size == 1
        formatter = "%[artist]/%[artist] - %[title].%[format]"
      else
        formatter = "%[artist]/%[artist] - %[title]-%[index].%[format]"
      end

      yield asset["image_url"].to_s,
        Service::Info.new(
          {
            "title"        => project["title"].to_s,
            "artist"       => project["user"]["full_name"].to_s,
            "format"       => URI.parse(asset["image_url"].to_s).path.not_nil!.split(".").last,
            "index"        => sprintf("%0#{project["assets"].as_a.size.to_s.size}i", i),
          },
          formatter
        )
    end
  end

  def download(url)
    puts "fetching: " + url
    uri = URI.parse(url)

    get_user_projects uri.path.not_nil!.strip("/").split("/")[0] do |url, info|
      yield url, info
    end
  end
end
