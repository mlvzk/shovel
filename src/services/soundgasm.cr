class Soundgasm < Service::Base
  HOST = "soundgasm.net"

  def self.valid_target?(url)
    url.includes? HOST
  end

  private def get(url)
    uri = URI.parse(url)
    author = uri.path.not_nil!.strip("/").split("/")[1]
    response = HTTP::Client.get(uri).body
    abort "no response from #{uri.to_s}" unless response

    title = ""

    raw_url = XML.parse_html(response)
      .tap { |o|
        title = o.xpath_node("//div[@class='jp-title']")
          .not_nil!
          .content
      }
      .xpath_nodes("//script[@type='text/javascript']")
      .map(&.content)
      .find(&.includes? "$(document).ready(function(){")
      .not_nil!
      .match(/(?<=m4a: ")(.*?)(?=")/)
      .not_nil![0]
    abort "couldn't parse page" unless raw_url

    yield raw_url,
      Service::Info.new(
        {
          "artist" => author,
          "title" => title,
          "ext" => URI.parse(raw_url).path.not_nil!.split(".").last,
        },
        "%[artist]/%[artist] - %[title].%[ext]"
    )
  end

  private def get_user(username)
    (uri = URI.parse "https://" + HOST).path = File.join({"/u", username})
    XML.parse_html(HTTP::Client.get(uri).body)
      .xpath_nodes("//div[@class='sound-details']/a")
      .each do |url|
      get(url["href"]) { |url, info| yield url, info }
    end
  end

  def download(url, &block)
    path = URI.parse(url).path.not_nil!.strip("/").split("/")
    case path.size
    when 2
      get_user(path[1]) { |url, info| yield url, info }
    when 3
      get(url) { |url, info| yield url, info }
    end
  end
end
