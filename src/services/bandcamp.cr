class Bandcamp < Service::Base
  HOST = "bandcamp.com"

  def self.valid_target?(url)
    url.includes? HOST
  end

  private def get_album(url)
    puts url
    data = HTTP::Client.get(URI.parse url).body
    artist = data.match(/(?<=artist: ")(.*?)(?=")/).not_nil![0]

    album = data.match(/(?<=album_title: ")(.*?)(?=")/).try &.[0]
    if album
      puts "#{artist} - #{album}"
      formatter = "%[artist]/%[album]/%[artist] - %[album] - %[index].%[title].%[ext]"
    else
      puts "#{artist}"
      formatter = "%[artist]/%[artist] - %[title].%[ext]"
    end

    tracks = data.match(/(?<=trackinfo: \[{)(.*?)(?=}\],)/).try &.[0]
      .split("},{")
      .map { |o| JSON.parse("{#{o}}") }

    return unless tracks

    tracks.each_with_index do |o, i|
      puts "#{o["title"].to_s}"

      info = {
            "title"  => o["title"].to_s,
            "artist" => artist,
            "ext"    => "mp3",
            "index"  => sprintf("%0#{tracks.size.to_s.size}i", i),
      }

      if album
        info.merge({ "album" => album })
      end

      yield o["file"]["mp3-128"].to_s,
        Service::Info.new(
          info,
          formatter
        )
    end
  end

  private def get_artist(username)
    uri = URI.parse "https://#{username}.#{HOST}"
    XML.parse_html(HTTP::Client.get(uri).body)
      .xpath_nodes("//div[@class='ipCellLabel1']/a")
      .each do |url|
        get_album "https://#{username}.#{HOST}#{url["href"]}" do |url, info|
          yield url, info
        end
      end
  end

  def download(url, &block)
    uri = URI.parse url
    case
    when uri.path.not_nil!.strip("/").split("/")[0] == "album"
      get_album(url) { |url, info| yield url, info }
    else
      get_artist(uri.host.not_nil!.split(".")[0]) { |url, info| yield url, info }
    end
  end
end
