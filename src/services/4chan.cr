class Fourchan < Service::Base
  def self.valid_target?(url)
    url.includes?("4chan.org/") || url.includes?("4channel.org/")
  end

  private def get_raw(url)
    body = HTTP::Client.get(url).body
    root = XML.parse_html(body)

    root.xpath_nodes("//div[@class = 'file']").map do |node|
      imgUrl = node.xpath_node("a[@class = 'fileThumb']").not_nil!["href"]
      ext = imgUrl.split('.').last
      titleNode = node.xpath_node("div[@class = 'fileText']/a").not_nil!

      if imgUrl.starts_with? "//"
        imgUrl = "https:" + imgUrl
      end

      {
        imgUrl,
        Service::Info.new(
          {
            "original_name" => titleNode["title"]? || titleNode.content,
            "ext"           => ext,
            "id"            => imgUrl.split('/').last.split('.').first,
          },
          "%[original_name]"
        ),
      }
    end
  end

  def download(url, &block)
    get_raw(url).each { |url, info| yield url, info }
  end
end
