class YouTube < Service::Base
  HOST       = "https://www.youtube.com"
  HOST_SHORT = "https://youtu.be"

  class_property tag = 250

  def self.valid_target?(url)
    url.includes? HOST
  end

  def get_video(id)
    uri = URI.parse id
    uri.query = HTTP::Params.encode(
      HTTP::Params.parse(uri.query.not_nil!).to_h.select({"v"})
    )

    script = XML.parse_html(HTTP::Client.get(uri).body)
      .xpath_nodes("//div[@id='player-mole-container']/script")
      .map(&.content)
      .find(&.includes? "ytplayer.config =")
      .not_nil!
      .match(/(?<=ytplayer\.config =)(.*?)(?=;ytplayer\.load = function())/)
      .not_nil![0]

    title = author = album = ext = ""

    raw_url = JSON.parse(
      JSON.parse(script)["args"]["player_response"].to_s
    )
      .tap { |o| o["videoDetails"].tap { |o|
        author = o["author"].to_s
        title = o["title"].to_s
      } }
      .["streamingData"].as_h.values_at("formats", "adaptiveFormats")
      .to_a.map(&.as_a).flatten
      .find { |itag| itag["itag"] == @@tag }
      .not_nil!
      .tap { |itag| ext = itag["mimeType"].to_s.split(";").first.split("/").last }
      .["url"].to_s

    yield raw_url,
      Service::Info.new(
        {
          "title"   => title,
          "channel" => author,
          "ext"     => ext,
        },
        "%[channel]/%[title].%[ext]"
      )
  end

  def download(url)
    get_video url do |url, info|
      yield url, info
    end
  end
end
