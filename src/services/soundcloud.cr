class SoundCloud < Service::Base
  HOST     = "soundcloud.com"
  ENDPOINT = "https://api.soundcloud.com"

  # thanks scdl
  CLIENT_ID = "a3e059563d7fd3372b49b37f00a00bcf"

  def self.valid_target?(url)
    url.includes? HOST
  end

  def get_track(url)
    (uri = URI.parse ENDPOINT).path = "/resolve"
    uri.query = HTTP::Params.encode(
      {
        "client_id" => CLIENT_ID,
        "url"       => url,
      }
    )
    data = HTTP::Client.get(uri).body
    track_location = JSON.parse(data)["location"].to_s
    track_data = HTTP::Client.get(track_location).body
    track_json = JSON.parse(track_data)

    if track_json["download_url"]?
      download_uri = URI.parse(track_json["download_url"].to_s)
      download_uri.query = HTTP::Params.encode({"client_id" => CLIENT_ID})

      response = HTTP::Client.get(download_uri)

      if response.status_code == 302
        # puts "downloading original"
        location = JSON.parse(response.body)["location"].to_s
      else
        location = nil
      end
    end

    unless location
      # puts "downloading raw stream"
      stream_uri = URI.parse(track_json["stream_url"].to_s)
      stream_uri.query = HTTP::Params.encode({
        "client_id" => CLIENT_ID,
      })

      stream_data = HTTP::Client.get(stream_uri).body
      location = JSON.parse(stream_data)["location"].to_s
    end

    yield location,
      Service::Info.new(
        {
          "artist" => track_json["user"]["username"].to_s,
          "title" => track_json["title"].to_s,
          "ext" => URI.parse(location).path.not_nil!.split(".").last,
        },
        "%[artist]/%[artist] - %[title].%[ext]"
      )
  end

  def download(url)
    get_track url do |url, info|
      yield url, info
    end
  end
end
