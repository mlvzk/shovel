class NHentai < Service::Base
  HOST = "nhentai.net"

  def self.valid_target?(url)
    url.includes? HOST
  end

  def get_pages(media_id)
    (uri = URI.parse "https://" + HOST).path = "/g/#{media_id}/"
    response = HTTP::Client.get(uri).body
    abort "no response from #{uri}" unless response

    gallery = JSON.parse(
      XML.parse_html(response)
        .xpath_nodes("//script")
        .map(&.content)
        .find(&.includes? "var gallery = new N.gallery(")
        .not_nil!
        .match(/(?<=var gallery = new N.gallery\()(.*?)(?=\);)/)
        .not_nil![0]
    )

    gallery.["images"]["pages"].as_a.map_with_index do |page, i|
      i += 1
      ext = "jpg"

      yield "https://i.#{HOST}/galleries/#{gallery["media_id"].to_s}/#{i}.jpg",
        Service::Info.new(
          {
            "media_id" => gallery["media_id"].to_s,
            "artists"  => gallery["tags"].as_a.group_by(&.["type"])["artist"].map(&.["name"]).join(", "),
            "title"    => gallery["title"]["english"].to_s,
            "pages"    => gallery["images"]["pages"].as_a.size.to_s,
            "index"    => sprintf("%0#{gallery["images"]["pages"].as_a.size.to_s.size}i", i),
            "format"   => ext,
            "ext"      => ext,
          },
          "%[artists]/%[media_id]-%[title]/%[index].%[ext]"
        )
    end
  end

  def download(url)
    uri = URI.parse url
    get_pages uri.path.not_nil!.strip("/").split("/")[1] do |url, info|
      yield url, info
    end
  end
end
