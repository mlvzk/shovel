class Imgur < Service::Base
  def self.valid_target?(url)
    url.includes? "imgur.com/"
  end

  def get_raw(url)
    body = HTTP::Client.get(url).body
    root = XML.parse_html(body)

    album_name = root.xpath_node("//div[@class = 'post-title-container']/h1").not_nil!.content
    if album_name.empty?
      album_name = "no-title"
    end

    image_containers = root.xpath_nodes("//div[@class = 'post-images']/div[@class = 'post-image-container']")
    image_containers.map_with_index do |image_container, i|
      case
      when image_container["itemtype"].includes? "VideoObject"
        ext = "mp4"
      else
        ext = "png"
      end

      if image_containers.size == 1
        formatter = "%[album].%[ext]"
      else
        formatter = "%[album]-%[index].%[ext]"

      end

      # TODO take src from meta[@contentURL] if available
      yield "https://i.imgur.com/#{image_container["id"]}.#{ext}",
        Service::Info.new(
          {
            "album" => album_name,
            "index" => sprintf("%0#{image_containers.size.to_s.size}i", i),
            "ext"   => ext,
          },
          formatter
      )
    end
  end

  def download(url)
    get_raw url do |url, info|
      yield url, info
    end
  end
end
