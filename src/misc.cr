def download(url : String, dst : String)
  unless File.exists? dst
    HTTP::Client.get(url) do |r|
      FileUtils.mkdir_p(File.dirname dst)
      File.open(dst, "w") do |file|
        copy(r.body_io, file) do |p|
          if r.headers["Content-Length"]?
            progress = p * 100 / r.headers["Content-Length"].to_i

            size = Terminal.get_size.ws_col

            left = "["
            right = "] #{progress.to_s}%"
            size -= right.size + left.size
            bar = ("#" * (size * progress / 100))
              .+ (" " * (size - (size * progress / 100)))

            print "\r"
            print left + bar + right
          end
        end
        puts
      end
    end
  else
    puts "already downloaded: " + dst
  end
end

def retry(&block)
  loop do
    begin
      return yield
    rescue
    end
  end
end

def copy(src, dst)
  buffer = uninitialized UInt8[4096]
  count = 0
  while (len = src.read(buffer.to_slice).to_i32) > 0
    dst.write buffer.to_slice[0, len]
    count += len
    yield count
  end
  len < 0 ? len : count
end

module Terminal
  extend self

  lib C
    struct Winsize
      ws_row : UInt16    # rows, in characters
      ws_col : UInt16    # columns, in characters
      ws_xpixel : UInt16 # horizontal size, pixels
      ws_ypixel : UInt16 # vertical size, pixels
    end

    TIOCGWINSZ = 0x5413

    fun ioctl(fd : Int32,
              request : UInt32,
              winsize : C::Winsize*) : Int32
  end

  def get_size
    C.ioctl(0, C::TIOCGWINSZ, out screen_size) # magic number
    return screen_size
  end
end
